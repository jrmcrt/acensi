<?php

function minimumMoves($grid, $startX, $startY, $goalX, $goalY) {
	// $grid = array_map(function($c){//transform into matrix, not really useful i guess
	// 	return str_split($c);
	// }, $grid);

	$GLOBALS['map'] = $grid;//declare a global map
    return findExit([[$startY, $startX]], ["$startY $startX" => 1], 0, $goalX, $goalY);
    //call findExit function, we start with our starting coords as $neighbours and visited.

}

function findExit($neighbours, $visited, $count, $goalX, $goalY){
	$children = [];//initialize children array

	foreach($neighbours as $n){//loop over neighbours
		foreach(getNeighbours($n, $visited) as $child){//loop over each neighbours of $neighbours
			list($y, $x) = $child;//extract coords
			if(!isset($visited["$y $x"])){//if we didn't visited them already
				if($y === $goalY && $x === $goalX){//if their coord == goal coors we're good
					return $count + 1;//return nb of steps to get there
				}
				$visited["$y $x"] = 1;//add child to visited list so we don't loop over them again
				$children[] = $child;//add to list of children
			}
		}
	}

	return count($children) ? findExit($children, $visited, $count += 1, $goalX, $goalY) : 'not found';
	//if we have children, recall function with new list of children and updated list of visited node
}

function getNeighbours($coord, $visited){//find neighboords. so everything in straight line until it is stop by an obstacle
	//kinda similar to something we've done in the bomberman-like ai contest
	list($y, $x) = $coord;
	$neighbours = [];//initialize list of neighbours

	for($i=$x-1; $i >= 0; $i--){//will find all neighbours on the left
		if($GLOBALS['map'][$y][$i] == 'X')//that's an obstacle we break out of the loop
			break;
		// if(!in_array([$y, $i], $visited))
		if(!isset($visited["$y $i"]))//if the node is unvisited, add it to the neighbours list
			$neighbours[] = [$y, $i];
	}
	for($i=$x+1; $i < count($GLOBALS['map']); $i++){//right
		if($GLOBALS['map'][$y][$i] == 'X')
			break;
		// if(!in_array([$y, $i], $visited))
		if(!isset($visited["$y $i"]))
			$neighbours[] = [$y, $i];
	}
	for($i=$y-1; $i >= 0; $i--){//up
		if($GLOBALS['map'][$i][$x] == 'X')
			break;
		//if(!in_array([$i, $x], $visited))
		if(!isset($visited["$i $x"]))	
			$neighbours[] = [$i, $x];
	}
	for($i=$y+1; $i < count($GLOBALS['map']); $i++){//down
		if($GLOBALS['map'][$i][$x] == 'X')
			break;
		//if(!in_array([$i, $x], $visited))
		if(!isset($visited["$i $x"]))
			$neighbours[] = [$i, $x];
	}

	return $neighbours;		
}

$start_time = microtime(1);
$grid = ['.X.', '.X.', '...'];
$result = minimumMoves($grid, 0, 0, 2, 0);
echo $result . "\n";
$end_time = microtime(1);
echo 'time : ' . ($end_time - $start_time) . "\n";
?>