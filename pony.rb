require 'benchmark'

def copy(var)
    Marshal.load(Marshal.dump(var))
end

def composable(str, words)
  composable_aux(str, words, {}, [])
end

def composable_aux(str, words, memo, result)
  if str == ""                # The base case
    puts 'result : ' + result*' '
    return true
  end  
  return memo[str] unless memo[str].nil?  # Return the answer if we already know it

  memo[str] = false              # Assume the answer is `false`

  words.each do |word|           # For each word in the list:
    length = word.length
    start  = str[0..length-1]
    rest   = str[length..-1]

    # If the test string starts with this word,
    # and the remaining part of the test string
    # is also composable, the answer is true.
    tempResult = copy(result)
    tempResult << word
    if start == word and composable_aux(rest, words, memo, tempResult)
      memo[str] = true           # Mark the answer as true
      return true #improvements ?
    end
  end

  memo[str]                      # Return the answer
end


tests = [['because can do must we what', 'wedowhatwemustbecausewecan'],['hello planet', 'helloword'],['ab abcd cd', 'abcd']];
puts Benchmark.measure{ 
    0.upto(10000){
          tests.each do |test|
            words, str = test
            p composable(str, words.split)
          end
    }
} * 1000 

