<?php
$s = microtime(1);

$n = 5;
$k = 30;
$dna = 'AAAAD';

$mutations['A'] = ['A' => 'A', 'B' => 'B', 'C' => 'C', 'D' => 'D'];
$mutations['B'] = ['A' => 'B', 'B' => 'A', 'C' => 'D', 'D' => 'C'];
$mutations['C'] = ['A' => 'C', 'B' => 'D', 'C' => 'A', 'D' => 'B'];
$mutations['D'] = ['A' => 'D', 'B' => 'C', 'C' => 'B', 'D' => 'A'];

$result = [];
$cycle = false;
$temp = $dna;
for($i=0; $i<$k; $i++){
	if(isset($cache["$dna"])){
        $cycle = true;
        $next = $cache["$dna"];
        $dna = $next;
        $c = microtime(1);
		echo "time : " . ($c - $s) . "\n";
		$periodL = $i;
		echo "period length : $periodL\n";
		$remaining = $k - $periodL;
		echo "remaining $remaining\n";
		$modulo = $remaining % $periodL;
		echo "modulo $modulo\n";
		echo $result[$modulo];
		//var_dump($cache);
		die();
	}
    else{
		for($j=0; $j<$n; $j++)
			$temp[$j] = $mutations[$dna[$j]][$dna[($j + 1) % $n]];

	    $cache["$dna"] = $temp;
	    $result[] = $temp;
		$dna = $temp; 
    }
	echo ($i+1) . " - $dna \n";
}

echo "result : $dna\n";
echo $cycle ? "cycle\n" : "nope\n";

$e = microtime(1);
echo "time : " . ($e - $s) . "\n";