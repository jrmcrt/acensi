<?php
global $found;
$time_start = microtime(1);

$tests = [['because can do must we what', 'wedowhatwemustbecausewecan'],['hello planet', 'helloword'],['ab abcd cd', 'abcd']];
foreach ($tests as $key => $value) {
	list($passwords, $pass) = $value;
	$GLOBALS['found'][$passwords] = false;
	$passwords = explode(' ', $passwords);
	solve($passwords, $pass, []);
	if(!$GLOBALS['found'][join(' ', $passwords)])
		echo "WRONG PASSWORD\n";
}

function solve($passwords, $pass, $result){
	if($GLOBALS['found'][join(' ', $passwords)])
		return true;
	if(!strlen($pass)){
		echo join(' ', $result) . "\n";
		$GLOBALS['found'][join(' ', $passwords)] = true;
	}
	else{
		foreach($passwords as $p){
			$index = strpos($pass, $p);
			if($index === 0){
				$passTemp = substr($pass, strlen($p));
				$resultTemp = $result;
				$resultTemp[] = $p;
				solve($passwords, $passTemp, $resultTemp);
			}
		}
	}
}

$time_end = microtime(1);
echo ($time_end - $time_start) * 1000 . " milliseconds\n";


